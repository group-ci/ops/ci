FROM registry.gitlab.com/group-ci/images/java:11.0.9

ARG JAR_FILE
WORKDIR /app/
RUN mkdir -p /app/
ENV DEFAULT_JAVA_HEAP_OPTS="-XX:+AlwaysPreTouch"
ENV JAVA_HEAP_OPTS="-Xmx2g -Xms2g"
ENV DEFAULT_JAVA_OPTS="-server -Dfile.encoding=UTF8 -Djava.net.preferIPv4Stack=true -verbosegc -XX:+UseG1GC -XX:MaxGCPauseMillis=50 -Xlog:gc*,safepoint:./logs/gc.log:time,uptime:filecount=10,filesize=100M -XX:+HeapDumpOnOutOfMemoryError -XX:+PrintFlagsFinal"
ENV JAVA_OPTS=""
COPY ${JAR_FILE} /app/app.jar

ENTRYPOINT ["sh", "-c", "java ${DEFAULT_JAVA_OPTS} ${DEFAULT_JAVA_HEAP_OPTS} ${JAVA_HEAP_OPTS} ${JAVA_OPTS} -jar /app/app.jar"]
